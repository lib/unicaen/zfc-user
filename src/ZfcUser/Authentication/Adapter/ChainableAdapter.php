<?php

namespace ZfcUser\Authentication\Adapter;

use Laminas\Authentication\Storage\StorageInterface;
use Laminas\EventManager\EventInterface;

interface ChainableAdapter
{
    /**
     * @param EventInterface $e
     * @return bool
     */
    public function authenticate(EventInterface $e);

    /**
     * @return StorageInterface
     */
    public function getStorage();
}
