CHANGELOG
=========

6.1.0 (29/01/2024)
------------------

- Suppression de la dépendance à laminas-crypt.
- ZfcUser\Password\Bcrypt remplace l'ancienne Laminas\Crypt\Password\Bcrypt.
- Si laminas-crypt reste nécessaire dans des projets, il faudra y ajouter "laminas/laminas-crypt": "^3.6"


6.0.2 (22/11/2024)
------------------

- [Fix] Retrait de la dépendance à laminas-loader, inutile ici

6.0.1
-----
- retrait de la dépendance à laminas-dependency-plugin

6.0.0
-----
- Compatibilité PHP8.2 (compatible PHP8.0)

5.0.0
-----
- PHP 8 requis

4.0.1 (10/12/2021)
------------------
- PHP 7.4 minimum requis

4.0.0
-----
- Changelog ;)
- Passage de Zend à Laminas
